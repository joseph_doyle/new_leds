void sideSwap(double color, int shortDel, int longDel) {
  if (isFirst) {
    startMillis = millis();
    isFirst = false;
  }
  loopTimeMillis = millis() - startMillis;
  if (loopTimeMillis <= shortDel) {                 //left side on
    setLeft(color);
  }
  else if (between(shortDel, (2 * shortDel))) {      //all off
    colorWipe(OFF);
  }
  else if (between((2 * shortDel), (3 * shortDel))) {   //left side on
    setLeft(color);
  }
  else if (between((3 * shortDel), (4 * shortDel))) {    //all off
    colorWipe(OFF);
  }
  else if (between((4 * shortDel), (4 * shortDel + longDel))) {   //left side on long
    setLeft(color);
  }
  else if (between((4 * shortDel + longDel), (5 * shortDel + longDel))) {   //all off
    colorWipe(OFF);
  }

  else if (between((5 * shortDel + longDel), (6 * shortDel + longDel))) {   //right side on
    setRight(color);
  }
  else if (between((6 * shortDel + longDel), (7 * shortDel + longDel))) {   //all off
    colorWipe(OFF);
  }
  else if (between((7 * shortDel + longDel), (8 * shortDel + longDel))) {   //right on
    setRight(color);
  }
  else if (between((8 * shortDel + longDel), (9 * shortDel + longDel))) {   //all off
    colorWipe(OFF);
  }
  else if (between((9 * shortDel + longDel), (9 * shortDel + 2 * longDel))) { //right on long
    setRight(color);
  }
  else if (between((9 * shortDel + 2 * longDel), (10 * shortDel + 2 * longDel))) { //all off
    colorWipe(OFF);
  }
  else {
    isFirst = true;
  }
}
