#include <OctoWS2811.h>

//175 on jeep
const int ledsPerleds = 175;

DMAMEM int displayMemory[ledsPerleds * 6];
int drawingMemory[ledsPerleds * 6];

const int config = WS2811_GRB | WS2811_800kHz;

OctoWS2811 leds(ledsPerleds, displayMemory, drawingMemory, config);

char val;

// define number of leds in the:
#define leftRock 175     // left side rock light bar 175 on jeep
#define rightRock 175    // right side rock bar 175 on jeep
#define leftFoot 0      // left footwell
#define rightFoot 0     // right footwell

int rock = leftRock + rightRock;

int lOne = 0;
int lTwo = 1;
int lThree = 2;
int lFour = 3;
int lFive = 4;
int rOne = 175;
int rTwo = 176;
int rThree = 177;
int rFour = 178;
int rFive = 179;

int color, x, y, wait;

#define RED    0xFF0000
#define GREEN  0x00FF00
#define BLUE   0x0000FF
#define WHITE  0xFFFFFF
#define OFF    0x000000
#define JBLUE  0x033DD7
#define PURPLE 0x2F002F
#define ORANGE 0xF91B01
#define AMBER  0xF96600
#define DWHITE 0x131313

unsigned long startMillis = 0;        // will store millis of when a loop started
unsigned long loopTimeMillis = 0;
unsigned long currentMillis = 0;

bool isFirst = true;      // will store if it's the first time a millis controlled loop is run

int rainbowColors[180];
int rain[720];

void setup() {
  for (int i = 0; i < 180; i++) {
    int hue = i * 2;
    int saturation = 100;
    int lightness = 50;
    // pre-compute the 180 rainbow colors
    rainbowColors[i] = makeColor(hue, saturation, lightness);
  }
  //load rainbow colors into rain 4 times for referencing
  for (int i=0; i<180; i++){
    rain[i]=rainbowColors[i];
  }
  for (int i=180; i<360; i++){
    rain[i]=rainbowColors[i];
  }
  for (int i=360; i<540; i++){
    rain[i]=rainbowColors[i];
  }
  for (int i=540; i<720; i++){
    rain[i]=rainbowColors[i];
  }
  
  Serial.begin(57600);
  leds.begin();
}

void loop() {
  Serial.println(millis());
  if (Serial.available()) {
    val = Serial.read(); // read it and store it in val
  }

  currentMillis = millis();

  /////////////////////////////////////   red   /////////////////////////////////////////////////

  if (val == 'r') {
    colorWipe(RED);
  }

  ///////////////////////////////////////   green   ///////////////////////////////////////////

  else if (val == 'g') {
    colorWipe(GREEN);
  }

  /////////////////////////////////////   blue   //////////////////////////////////////

  else if (val == 'b') {
    colorWipe(BLUE);
  }

  ////////////////////////////////   jeep blue   //////////////////////////////////////

  else if (val == 'B') {
    colorWipe(JBLUE);
  }

  /////////////////////////////////////   purple   //////////////////////////////////

  else if (val == 'p') {
    colorWipe(PURPLE);
  }

  /////////////////////////////////////   orange   ////////////////////////////////// why were there two values?... I took one out hope it works still

  else if (val == 'o') {
    colorWipe(ORANGE);
  }

  //////////////////////////////////   amber   //////////////////////////////////

  else if (val == 'f') {
    colorWipe(AMBER);
  }

  /////////////////////////////////////   white   //////////////////////////////////

  else if (val == 'w') {
    colorWipe(WHITE);
  }

  ////////////////////////////////   segment switching amber   //////////////////////////////////

  else if (val == 'a') {
    segSwitch(AMBER, 250);
  }

  /////////////////////////////////   side swapping amber   ////////////////////////////////

  else if (val == 'c') {
    sideSwap(AMBER, 50, 300);
  }

  //////////////////////////////////   full blink amber/white   //////////////////////////////////

  else if (val == 'd') {
    doubleFlash(AMBER, 600, 50);
  }

  //////////////////////////////////   front to back amber   //////////////////////////////////

  else if (val == 'z') {
    ftb(50, AMBER);
  }

  ////////////////////////////////   segment switching orange   //////////////////////////////////

  else if (val == 'h') {
    segSwitch(ORANGE, 250);
  }

  /////////////////////////////////   side swapping orange   ////////////////////////////////

  else if (val == 'j') {
    sideSwap(ORANGE, 50, 300);
  }

  //////////////////////////////////   full blink orange/white   //////////////////////////////////

  else if (val == 'k') {
    doubleFlash(ORANGE, 600, 50);
  }

  //////////////////////////////////   front to back orange   //////////////////////////////////

  else if (val == 'm') {
    ftb(ORANGE, 0);
  }

  //////////////////////////////////   police   //////////////////////////////////

  else if (val == 'e') {
    police(50);
  }

  //////////////////////////////////   red white and blue   //////////////////////////////////

  else if (val == '4') {
    redWhiteBlue(1000);
  }

  //////////////////////////////////   rFREEEEEEDOOOOMMMMMMM   //////////////////////////////////

  else if (val == 'F') {
    freedom(200);
  }

  else if (val == 'T') {
    lameMysteryMachine(PURPLE, GREEN, 1000);
  }

  else if (val == 'E') {
    litMysteryMachine(PURPLE, GREEN);
  }

  else if (val == 'R') {
    rainbow(30);
  }

  /////////////////////////////////////////  else  ////////////////////////////////////////////

  else {
    darkStrip();
  }
}
