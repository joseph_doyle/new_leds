void litMysteryMachine(double color1, double color2){
  if (isFirst) {
    startMillis = millis();
    isFirst = false;
  }
  loopTimeMillis = millis() - startMillis;
  if (loopTimeMillis <= 50) {
    setLeft(color1);
  }
  else if (between(50, 100)) {       //all off
    darkStrip();
  }
  else if (between(100, 150)) {      //left side on color1
    setLeft(color1);
  }
  else if (between(150, 200)) {       //all off
    darkStrip();
  }
  else if (between(200, 250)) {        //left side on color1
    setLeft(color1);
  }
  else if (between(250, 300)) {          //all off
    darkStrip();
  }

  else if (between(300, 350)) {          //right side on color1
    setRight(color1);
  }
  else if (between(350, 400)) {          //all off
    darkStrip();
  }
  else if (between(400, 450)) {          //right on color1
    setRight(color1);
  }
  else if (between(450, 500)) {          //all off
    darkStrip();
  }
  else if (between(500, 550)) {         //right on color1
    setRight(color1);
  }
  else if (between(550, 600)) {        //all off
    darkStrip();
  }

  else if (between(600, 650)) {      //left side on color2
    setLeft(color2);
  }
  else if (between(650, 700)) {       //all off
    darkStrip();
  }
  else if (between(700, 750)) {      //left side on color2
    setLeft(color2);
  }
  else if (between(750, 800)) {       //all off
    darkStrip();
  }
  else if (between(800, 850)) {        //left side on color2
    setLeft(color2);
  }
  else if (between(850, 900)) {          //all off
    darkStrip();
  }

  else if (between(900, 950)) {          //right side on color2
    setRight(color2);
  }
  else if (between(950, 1000)) {          //all off
    darkStrip();
  }
  else if (between(1000, 1050)) {          //right on color2
    setRight(color2);
  }
  else if (between(1050, 1100)) {          //all off
    darkStrip();
  }
  else if (between(1100, 1150)) {         //right on color2
    setRight(color2);
  }
  else if (between(1150, 1200)) {        //all off
    darkStrip();
  }

  else {
    isFirst = true;
  }
  
}

