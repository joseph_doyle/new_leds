void redWhiteBlue(int del) {
  if (isFirst) {
    startMillis = millis();
    isFirst = false;
  }
  loopTimeMillis = millis() - startMillis;
  if (loopTimeMillis <= del) {
    colorWipe(RED);
  }
  else if (between(del, 2 * del)) {
    colorWipe(WHITE);
  }
  else if (between(2 * del, 3 * del)) {
    colorWipe(BLUE);
  }
  else {
    isFirst = true;
  }
}
