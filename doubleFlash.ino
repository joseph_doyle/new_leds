/*********************************************************/
/*Flash takes a color and two times in msec, turns all   */
/*LEDs to the first color for the first amount of time,  */
/*turns white for the second amount of time, turns back  */
/*for the second amount of time, then back white again   */
/*for the second amount of time. Usually used with large */
/*and small number for an oooooooooon off on off pattern */
/*********************************************************/

void doubleFlash(double color, int longtime, int shorttime ) {
  if (isFirst) {
    startMillis = millis();
    isFirst = false;
  }
  loopTimeMillis = millis() - startMillis;
  if (loopTimeMillis <= longtime) {
    colorWipe(color);
  }
  else if (between(longtime, (longtime + shorttime))) {
    colorWipe(DWHITE);
  }
  else if (between(longtime + shorttime, (shorttime * 2 + longtime))) {
    colorWipe(color);
  }
  else if (between((shorttime * 2 + longtime), (shorttime * 3 + longtime))) {
    colorWipe(DWHITE);
  }
  else {
    isFirst = true;
  }
}

