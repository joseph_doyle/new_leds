void police(int del) {
  if (isFirst) {
    startMillis = millis();
    isFirst = false;
  }
  loopTimeMillis = millis() - startMillis;
  if (loopTimeMillis <= 50) {
    setLeft(RED);
  }
  else if (between(50, 100)) {       //all off
    darkStrip();
  }
  else if (between(100, 150)) {      //left side on red
    setLeft(RED);
  }
  else if (between(150, 200)) {       //all off
    darkStrip();
  }
  else if (between(200, 250)) {        //left side on red
    setLeft(RED);
  }
  else if (between(250, 300)) {          //all off
    darkStrip();
  }

  else if (between(300, 350)) {          //right side on red
    setRight(RED);
  }
  else if (between(350, 400)) {          //all off
    darkStrip();
  }
  else if (between(400, 450)) {          //right on red
    setRight(RED);
  }
  else if (between(450, 500)) {          //all off
    darkStrip();
  }
  else if (between(500, 550)) {         //right on red
    setRight(RED);
  }
  else if (between(550, 600)) {        //all off
    darkStrip();
  }

  else if (between(600, 650)) {      //left side on blue
    setLeft(BLUE);
  }
  else if (between(650, 700)) {       //all off
    darkStrip();
  }
  else if (between(700, 750)) {      //left side on blue
    setLeft(BLUE);
  }
  else if (between(750, 800)) {       //all off
    darkStrip();
  }
  else if (between(800, 850)) {        //left side on blue
    setLeft(BLUE);
  }
  else if (between(850, 900)) {          //all off
    darkStrip();
  }

  else if (between(900, 950)) {          //right side on blue
    setRight(BLUE);
  }
  else if (between(950, 1000)) {          //all off
    darkStrip();
  }
  else if (between(1000, 1050)) {          //right on blue
    setRight(BLUE);
  }
  else if (between(1050, 1100)) {          //all off
    darkStrip();
  }
  else if (between(1100, 1150)) {         //right on blue
    setRight(BLUE);
  }
  else if (between(1150, 1200)) {        //all off
    darkStrip();
  }

  else {
    isFirst = true;
  }
}
