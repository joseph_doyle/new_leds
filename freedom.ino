void freedom(int del) {
  if (isFirst) {
    startMillis = millis();
    isFirst = false;
  }
  loopTimeMillis = millis() - startMillis;
  if (loopTimeMillis <= del) {
    setLeft(RED);
    setRight(WHITE);
  }
  else if (between(del, 2 * del)) {     //all off
    darkStrip();
  }
  else if (between(2 * del, 3 * del)) {  //left side on red
    setLeft(RED);
    setRight(WHITE);
  }
  else if (between(3 * del, 4 * del)) {   //all off
    darkStrip();
  }
  else if (between(4 * del, 5 * del)) {    //left side on red
    setLeft(RED);
    setRight(WHITE);
  }
  else if (between(5 * del, 6 * del)) {      //all off
    darkStrip();
  }

  else if (between(6 * del, 7 * del)) {      //right side on red
    setLeft(WHITE);
    setRight(BLUE);
  }
  else if (between(7 * del, 8 * del)) {      //all off
    darkStrip();
  }
  else if (between(8 * del, 9 * del)) {      //right on red
    setLeft(WHITE);
    setRight(BLUE);
  }
  else if (between(9 * del, 10 * del)) {      //all off
    darkStrip();
  }
  else if (between(10 * del, 11 * del)) {     //right on red
    setLeft(WHITE);
    setRight(BLUE);
  }
  else if (between(11 * del, 12 * del)) {    //all off
    darkStrip();
  }

  else if (between(12 * del, 13 * del)) {  //left side on blue
    setLeft(BLUE);
    setRight(RED);
  }
  else if (between(13 * del, 14 * del)) {   //all off
    darkStrip();
  }
  else if (between(14 * del, 15 * del)) {  //left side on blue
    setLeft(BLUE);
    setRight(RED);
  }
  else if (between(15 * del, 16 * del)) {   //all off
    darkStrip();
  }
  else if (between(16 * del, 17 * del)) {    //left side on blue
    setLeft(BLUE);
    setRight(RED);
  }
  else if (between(17 * del, 18 * del)) {      //all off
    darkStrip();
  }

  else {
    isFirst = true;
  }
}
