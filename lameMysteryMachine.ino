void lameMysteryMachine(double color1, double color2, int del) {
  if (isFirst) {
    startMillis = millis();
    isFirst = false;
  }
  loopTimeMillis = millis() - startMillis;
  if (loopTimeMillis <= del) {
    setLeft(color1);
    setRight(color2);
  }
  else if (between(del, (2 * del))) {
    setLeft(color2);
    setRight(color1);
  }
  else {
    isFirst = true;
  }
}

