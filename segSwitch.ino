void segSwitch(double color, int del) {
  if (isFirst) {
    startMillis = millis();
    isFirst = false;
  }
  loopTimeMillis = millis() - startMillis;
  if (loopTimeMillis <= del) {
    for (uint16_t i = 0; i < 43; i++) {
      leds.setPixel(i, color);
    }
    for (uint16_t i = 43; i < 87; i++) {
      leds.setPixel(i, OFF);
    }
    for (uint16_t i = 87; i < 131; i++) {
      leds.setPixel(i, color);
    }
    for (uint16_t i = 131; i < 175; i++) {
      leds.setPixel(i, OFF);
    }
    for (uint16_t i = 175; i < 218; i++) {
      leds.setPixel(i, color);
    }
    for (uint16_t i = 218; i < 262; i++) {
      leds.setPixel(i, OFF);
    }

    for (uint16_t i = 262; i < 306; i++) {
      leds.setPixel(i, color);
    }
    for (uint16_t i = 306; i < 350; i++) {
      leds.setPixel(i, OFF);
    }
    leds.show();
  }

  else if (between(del, (2 * del))) {
    for (uint16_t i = 0; i < 43; i++) {
      leds.setPixel(i, OFF);
    }
    for (uint16_t i = 43; i < 87; i++) {
      leds.setPixel(i, color);
    }
    for (uint16_t i = 87; i < 131; i++) {
      leds.setPixel(i, OFF);
    }
    for (uint16_t i = 131; i < 175; i++) {
      leds.setPixel(i, color);
    }
    for (uint16_t i = 175; i < 218; i++) {
      leds.setPixel(i, OFF);
    }
    for (uint16_t i = 218; i < 262; i++) {
      leds.setPixel(i, color);
    }
    for (uint16_t i = 262; i < 306; i++) {
      leds.setPixel(i, OFF);
    }
    for (uint16_t i = 306; i < 350; i++) {
      leds.setPixel(i, color);
    }
    leds.show();
  }

  else {
    isFirst = true;
  }
}

