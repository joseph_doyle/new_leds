void colorWipe(int color)
{
  for (int i = 0; i < leds.numPixels(); i++) {
    leds.setPixel(i, color);
  }
  leds.show();
}

void setLeft(int color)                          //sets left side to color, right to OFF, then pushes to LEDs
{
  for (uint16_t i = 0; i < leftRock; i++) {
    leds.setPixel(i, color);
  }
  for (uint16_t i = leftRock; i < rock; i++) {
    leds.setPixel(i, OFF);
  }
  leds.show();
}

void setRight(int color)                         //sets right side to color, left to OFF, then pushes to LEDs
{
  for (uint16_t i = leftRock; i < rock; i++) {
    leds.setPixel(i, color);
  }
  for (uint16_t i = 0; i < leftRock; i++) {
    leds.setPixel(i, OFF);
  }
  leds.show();
}

void darkStrip()                                 //sets all LEDs OFF then pushes to strips
{
  for (int i = 0; i < leds.numPixels(); i++) {
    leds.setPixel(i, OFF);
  }
  leds.show();
}

bool between(int x, int y) {                      //checks if the loopTimeMillis is between two ints
  if (loopTimeMillis > x && loopTimeMillis <= y) {
    return true;
  }
  else {
    return false;
  }
}
