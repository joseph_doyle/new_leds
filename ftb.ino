void ftb(double color, int del) {
  colorWipe(OFF);
  if (lFive < 175) {
    leds.setPixel(lOne, color);
    leds.setPixel(lTwo, color);
    leds.setPixel(lThree, color);
    leds.setPixel(lFour, color);
    leds.setPixel(lFive, color);
    leds.setPixel(rOne, color);
    leds.setPixel(rTwo, color);
    leds.setPixel(rThree, color);
    leds.setPixel(rFour, color);
    leds.setPixel(rFive, color);
    leds.show();

    //delay(5);                                         //TEST THIS

    lOne = lOne + 1;
    lTwo = lTwo + 1;
    lThree = lThree + 1;
    lFour = lFour + 1;
    lFive = lFive + 1;
    rOne = lOne + 1;
    rTwo = rTwo + 1;
    rThree = rThree + 1;
    rFour = rFour + 1;
    rFive = rFive + 1;
  }
  else {
    lOne = 0;
    lTwo = 1;
    lThree = 2;
    lFour = 3;
    lFive = 4;
    rOne = 175;
    rTwo = 176;
    rThree = 177;
    rFour = 178;
    rFive = 179;
  }
}

